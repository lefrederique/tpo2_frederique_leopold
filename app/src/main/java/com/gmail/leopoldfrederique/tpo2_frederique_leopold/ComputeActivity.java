package com.gmail.leopoldfrederique.tpo2_frederique_leopold;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class ComputeActivity extends AppCompatActivity {

    TextView textViewNb1;
    TextView textViewNb2;

    Button sum;
    Button minus;
    Button multiply;
    Button divide;
    static int RESULT_MINE = 400;
    String nb1;
    String nb2;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compute);
        initViews();
        setUpViews();
    }


    /**
     * Methods that allow to retrieve the instance of views in the layout
     */
    private void initViews() {
        textViewNb1 = findViewById(R.id.textNb1);
        textViewNb2 = findViewById(R.id.textNb2);

        sum=findViewById(R.id.sum);
        minus=findViewById(R.id.minus);
        multiply=findViewById(R.id.multiply);
        divide=findViewById(R.id.divide);


        sum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double val1 = Double.parseDouble(nb1.trim());
                Double val2 = Double.parseDouble(nb2.trim());
                Double resultat= val1+val2;
                callMainActivity(resultat.toString(),1, null);
            }
        });

        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double val1 = Double.parseDouble(nb1.trim());
                Double val2 = Double.parseDouble(nb2.trim());
                Double resultat= val1-val2;
                callMainActivity(resultat.toString(),1, null);
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double val1 = Double.parseDouble(nb1.trim());
                Double val2 = Double.parseDouble(nb2.trim());
                Double resultat= val1*val2;
                callMainActivity(resultat.toString(),1, null);
            }
        });

        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double val1 = Double.parseDouble(nb1.trim());
                Double val2 = Double.parseDouble(nb2.trim());
                if(val2==0.0){
                    callMainActivity("0.0", 0, "Le dénominateur doit etre superieur a 0");
                }else{
                    Double resultat= val1/val2;
                    callMainActivity(resultat.toString(), 1, null);
                }
            }
        });

    }

    private void setUpViews() {
        Intent intent = getIntent();
        nb1 = intent.getStringExtra("nb1");
        nb2 = intent.getStringExtra("nb2");

        textViewNb1.setText("Nombre 1 : "+ nb1);
        textViewNb2.setText("Nombre 2 : "+ nb2);
    }

    private void callMainActivity(String resultat, int code_error, String message_erro){
        Intent intent = new Intent();

        if(code_error==1){
            intent.putExtra("result", resultat);
            setResult(RESULT_OK, intent);
        }else{
            intent.putExtra("result", "0");
            intent.putExtra("message_error", message_erro);
            setResult(RESULT_MINE, intent);
        }

        finish();
    }
}
